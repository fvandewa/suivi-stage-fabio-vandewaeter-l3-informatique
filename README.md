# Suivi stage Fabio Vandewaeter L3 informatique

## Contexte

Le stage se déroule dans l'équipe de recherche EVREF (https://www.inria.fr/fr/evref) , est encadré par Nicolas Anquetil et porte sur la génération automatique de tests

Il s'agit en réalité du sujet de thèse du doctorant Gabriel Darbord, donc en plus de l'aider dans le développement de ce projet c'est aussi l'occasion de voir comment se déroule et est rédigée une thèse

Tout se fait grâce à Pharo (que vous connaissez bien mieux que moi si je ne me trompe pas) et Moose, en se basant sur des métamodèles pour générer des tests indépendamment du langage ciblé

## Sujet de stage


L'objectif du stage est de réussir à générer des tests pour des programmes qui fonctionnent correctement actuellement mais qui doivent être mis à jour, afin de s'assurer qu'après cette modification le logiciel ait le même comportement qu'avant et que de nouveaux bugs ne soient pas apparus

Nous commençons par récupérer des informations sur le fonctionnement actuel du programme en mémorisant sous forme de traces les appels de fonctions qui sont fait et le résultat obtenu, puis nous créons des tests pour chaque appel, en vérifiant que le résultat est le même que celui obtenu par le programme

Grâce à des métamodèles il est possible d'utiliser ce programme sur différents langages de programmation (notamment Java et Pharo)

Les tests produits sont compréhensibles par des humains et permettent donc de repérer facilement un nouveau bug

Dépôt principal du projet : https://github.com/Evref-BL/Modest/tree/main

## Date de la visite

Lundi 27 Mai à 15h

## Date de la soutenance

? Lundi 10 Juin l'après-midi ?

## Résumés hebdomadaires
### Semaine 1
- présentation rapide avec les autres stagiaires de Pharo et d'un outil intégré pour gérer les dépôts gît
- discussion avec le doctorant Gabriel DARBORD pour présenter son sujet de thèse et ce que je vais faire
- travail sur des exercices pour s'améliorer en Pharo (implémentation d'une file prioritaire avec un tas pour améliorer une implémentation de l'algorithme de Dijkstra)
- réunion sur Moose :
-->  présentation par Anne Etien de ce qu'est un model et méta-model
--> présentation de Moose par Clotilde Toullec
- lecture de deux abstracts en lien avec le sujet de stage
- découverte des packages déjà crées par Gabriel Darbord
- Sprint le vendredi durant lequel toute l'équipe se réunit dans l'objectif de corriger des bugs dans Pharo

### Semaine 2
- présentation plus détaillée de comment sérialiser les objets pour créer des traces
- je dois créer un moyen de sérialiser en Jackson des objets Pharo : https://github.com/Modest-Project/PharoJackson
- création de FamixValuePharoJacksonImporter qui permet d'analyse les traces serialisées en Jackson et de créer des objets correspondant en lien avec le métamodèles
- amélioration de la sérialisation en Jackson
### Semaine 3
- amélioration de FamixValue2PharoVisitor, qui permet d'exporter des valeurs du model vers l'AST Pharo
- amélioration de FamixUTSUnitExporter qui nous permet de créer les packages, classes et méthodes de tests et donc de générer nos premiers tests en SUnit en y ajoutant les résultats de FamixValue2PharoVisitor
- enregistrement pour ESUG : https://esug.org/
### Semaine 4
- semaine en télétravail car Gabriel Darbord est en congés
- essayer de générer des tests sur un programme plus imposant que jusqu'à présent tel que DataFrame : https://github.com/PolyMathOrg/DataFrame
### Semaine 5
- retour de vacances de Gabriel Darbord et discussion sur ce que j'ai fait en son absence
- amélioration de la sérialisation en Jackson
- premiers tests générés pour DataFrame mais avec encore beaucoups d'erreurs car les blocks ne sont pas gérés lors de la sérialisation
- amélioration de la génération des tests qui permet d'avoir 1400 tests validés sur 1600
- j'essaie de générer des tests sur d'autres projets plus complexes pour vérifier que ça fonctionne bien mais il y a encore beaucoup de problèmes
- je fais des statistiques sur les tests (coverage et mutations score) pour un papier qu'écrit Gabriel Darbord dans le cadre de sa thèse
### Semaine 6
- génération des tests sur DataFrame et de statistiques sur les tests
- écriture des rapports de stages en français et anglais
- débuté écriture de la soutenance
- faire remplir la grille d'évaluation
- correction de problèmes compliqués dans les packages permettant de générer le code des tests
- création d'une classe DeepComparator pour comparer de façon recursive des objets sans faire d'égalité stricte
### Semaine 7
- visite du stage
- écriture des rapports de stages
- préparation de la soutenance
- j'ai fait relir mes rapports de stages et je commence à les envoyer
- sprint
### Semaine 8
- préparation de la soutenance
- détection des codes smells et tests smells générés par notre approche, dans le cadre d'un papier en rapport avec la thèse de Gabriel Darbord